﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace App.Scripts.UI
{
    public class UIManager: MonoBehaviour
    {
        [SerializeField] private List<UIPanel> _registerPanels;

        private static readonly List<UIPanel> Widgets = new List<UIPanel>();
        
        private void Awake()
        {
            foreach (var panel in _registerPanels)
            {
                Add(panel);
            }
            
            var panels = FindObjectsOfType<UIPanel>(true);
            foreach (var panel in panels)
            {
                Add(panel);
            }
        }

        public static void Add(UIPanel widget)
        {
            if (Widgets.Contains(widget)) return;
            Widgets.Add(widget);
        }

        public static T Get<T>() where T : UIPanel
        {
            var result = Widgets.First(i => i.GetType() == typeof(T));
            return (T) result;
        }
    }
}