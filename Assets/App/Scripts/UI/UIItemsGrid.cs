using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UI
{
    public class UIItemsGrid : MonoBehaviour
    {
        [SerializeField] private int _itemInRow = 5;
        [SerializeField] private Transform[] _rows;
        [SerializeField] private UIGridItem _uiGridItemPrefab;
        [SerializeField] private Toggle _hatToggle, _robeToggle, _skinToggle;
        [SerializeField] private ItemType _defaultType = ItemType.Robe;
        
        private void Start()
        {
            var rows = GetComponentsInChildren<HorizontalLayoutGroup>();
            _rows = new Transform[rows.Length];
            for (int i = 0; i < rows.Length; i++)
            {
                _rows[i] = rows[i].transform;
            }

            _hatToggle.onValueChanged.AddListener(val => { if (val) FillGrid(ItemType.Hat); });
            _robeToggle.onValueChanged.AddListener(val => { if (val) FillGrid(ItemType.Robe); });            
            _skinToggle.onValueChanged.AddListener(val => { if (val) FillGrid(ItemType.Skin); });

            FillGrid(_defaultType);
        }

        public void FillGrid(ItemType type)
        {
            ClearRows();
            Sprite[] resourceItems;
            switch (type)
            {
                case ItemType.Hat:
                    resourceItems = MainApp.GameResources.Hats;
                    break;
                case ItemType.Robe:
                    resourceItems = MainApp.GameResources.Robes;
                    break;
                case ItemType.Skin:
                    resourceItems = MainApp.GameResources.Skins;
                    break;
                default:
                    return;
            }

            for (int i = 0; i < MainApp.GameConfig.GridItemsCount; i++)
            {
                var rowIndex = i / _itemInRow;
                var gridItem = Instantiate(_uiGridItemPrefab, _rows[rowIndex]);
                if (i < resourceItems.Length)
                {
                    gridItem.Init(type, i);
                }
                else
                {
                    gridItem.Init();
                }
            }
        }

        [ContextMenu("ClearRows")]
        private void ClearRows()
        {
            foreach (var row in _rows)
            {
                foreach (Transform item in row)
                {
                    Destroy(item.gameObject);
                }
            }
        }
    }
}