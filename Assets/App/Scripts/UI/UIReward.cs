using System;
using App.Scripts.FortuneWheel;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UI
{
    public class UIReward : UIPanel
    {
        [SerializeField] private Image _image;
        [SerializeField] private TMP_Text _name;
        [SerializeField] private TMP_Text _count;

        public static event Action<PlayerInventory.Item> RewardAccepted;
        private static PlayerInventory.Item _lastReward;

        private void Awake()
        {
            GetComponent<Button>()?.onClick.AddListener(() =>
            {
                RewardAccepted?.Invoke(_lastReward);
                Hide();
            });
        }

        public void Show(Sprite sprite, string name)
        {
            _image.sprite = sprite;
            _name.text = name;
            if (_count != null) _count.text = string.Empty;
            gameObject.SetActive(true);
        }

        public void Show(Sprite sprite, string name, string count)
        {
            Show(sprite, name);
            if (_count != null) _count.text = count;
        }

        public void Show(PlayerInventory.Item item)
        {
            _lastReward = item;
            var sprite = MainApp.GameResources.GetSprite(item.Type, item.Index);
            if (item.Type != ItemType.Coins)
            {
                Show(sprite, item.Type.ToString());
            }
            else
            {
                Show(sprite, item.Type.ToString(), MainApp.GameConfig.BonusCoinsCount.ToString());
            }
        }
        
        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}