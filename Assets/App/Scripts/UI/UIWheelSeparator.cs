using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UI
{
    public class UIWheelSeparator : UIWidget
    {
        [SerializeField] private Image _image;
        [SerializeField] private TMP_Text _text;
        [SerializeField] private RectTransform _anchor;
        public RectTransform Anchor => _anchor;
        
        public void Init(Sprite sprite, string text)
        {
            _image.sprite = sprite;
            _text.text = text;
        }
    }
}
