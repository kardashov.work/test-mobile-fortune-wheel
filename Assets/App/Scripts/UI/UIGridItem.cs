using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts.UI
{
    public class UIGridItem : UIWidget
    {
        [SerializeField] private TMP_Text _priceText;
        [SerializeField] private Image _locked;
        [SerializeField] private Image _closed;
        [SerializeField] private Image _item;

        private bool _inInventory;
        private ItemType _type;
        private int _index;
        private Button _button;

        public static event Action<ItemType, int> ItemSelected;

        private void Start()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnClick);

            PlayerInventory.ItemAdded += PlayerInventoryOnItemAdded;
        }

        private void OnDestroy()
        {
            PlayerInventory.ItemAdded -= PlayerInventoryOnItemAdded;
        }
        
        private void PlayerInventoryOnItemAdded(PlayerInventory.Item item)
        {
            if (item.Type != _type || item.Index != _index) return;

            _locked.gameObject.SetActive(false);
            _closed.gameObject.SetActive(false);
        }

        private void OnClick()
        {
            if (_type == ItemType.None) return;

            if (MainApp.Inventory.Contains(_type, _index))
            {
                ItemSelected?.Invoke(_type, _index);
                return;
            }

            if (MainApp.Inventory.TryBuyItem(_type, _index))
            {
                _locked.gameObject.SetActive(false);
                _closed.gameObject.SetActive(false);
            }
        }

        public void Init()
        {
            _locked.gameObject.SetActive(true);
        }

        public void Init(ItemType type, int index)
        {
            _type = type;
            _index = index;

            _item.sprite = MainApp.GameResources.GetSprite(_type, _index);
            _priceText.text = MainApp.GameConfig.ItemPrice.ToString();
            _inInventory = MainApp.Inventory.Contains(type, index);
            if (!_inInventory)
            {
                _closed.gameObject.SetActive(true);
            }
        }
    }
}