using System;
using TMPro;

namespace App.Scripts.UI
{
    public class UISpinCountText : UIWidget
    {
        private TMP_Text _text;

        private void Start()
        {
            _text = GetComponent<TMP_Text>();
            _text.text = MainApp.GameConfig.MaxSpinCount.ToString();
            FortuneWheel.FortuneWheel.SpinCountChanged += FortuneWheelOnSpinCountChanged;
        }

        private void OnDestroy()
        {
            FortuneWheel.FortuneWheel.SpinCountChanged -= FortuneWheelOnSpinCountChanged;
        }

        private void FortuneWheelOnSpinCountChanged(int count)
        {
            _text.text = count.ToString();
        }
    }
}
