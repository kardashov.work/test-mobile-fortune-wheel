using System;
using TMPro;

namespace App.Scripts.UI
{
    public class UICoinsCountText : UIWidget
    {
        private TMP_Text _text;

        private void Start()
        {
            _text = GetComponent<TMP_Text>();
            _text.text = MainApp.GameConfig.StartupCoinsCount.ToString();
            PlayerInventory.CoinsCountChanged += FortuneWheelOnSpinCountChanged;
        }

        private void OnDestroy()
        {
            PlayerInventory.CoinsCountChanged -= FortuneWheelOnSpinCountChanged;
        }

        private void FortuneWheelOnSpinCountChanged(int count)
        {
            _text.text = count.ToString();
        }
    }
}
