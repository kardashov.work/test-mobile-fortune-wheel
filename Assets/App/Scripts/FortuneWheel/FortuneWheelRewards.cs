﻿using System;
using Random = UnityEngine.Random;

namespace App.Scripts.FortuneWheel
{
    public static class FortuneWheelRewards
    {
        private static readonly int RobesCount, HatsCount, SkinsCount;
        
        static FortuneWheelRewards()
        {
            HatsCount = MainApp.GameResources.Hats.Length;
            RobesCount = MainApp.GameResources.Robes.Length;
            SkinsCount = MainApp.GameResources.Skins.Length;
        }
        
        public static int GetRandomRobe() => Random.Range(0, RobesCount);
        public static int GetRandomHat() => Random.Range(0, HatsCount);
        public static int GetRandomSkin() => Random.Range(0, SkinsCount);

        public static int GetRandom(ItemType type)
        {
            switch (type)
            {
                case ItemType.Coins: return MainApp.GameConfig.BonusCoinsCount;
                case ItemType.Hat: return GetRandomHat();
                case ItemType.Robe: return GetRandomRobe();
                case ItemType.Skin: return GetRandomSkin();
            }

            return 0;
        }
    }
}