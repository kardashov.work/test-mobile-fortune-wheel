﻿using System;
using System.Collections.Generic;
using App.Scripts.ScriptableObjects;
using App.Scripts.UI;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

namespace App.Scripts.FortuneWheel
{
    public class FortuneWheel : BaseBehaviour
    {
        private const float FULL_ANGLE = 360f;
        
        [SerializeField] private float _pointerOffset;
        [SerializeField] private RectTransform _pointer;
        [SerializeField] private RectTransform _wheel;
        [SerializeField] private RectTransform _separatorPrefab;

        private readonly List<FortuneWheelItemData> _itemsData = new List<FortuneWheelItemData>();

        private bool _rotating;
        private static GameConfig Config => MainApp.GameConfig;

        public static event Action<PlayerInventory.Item> WheelStop;

        private FortuneWheelItemData _target;

        private static int _spinsCount;

        public static event Action<int> SpinCountChanged; 
        
        private void Start()
        {
            _spinsCount = MainApp.GameConfig.MaxSpinCount;
            
            var to = 0f;
            foreach (var item in Config.WheelItem)
            {
                var a = FromItemToAngle(item.Value);
                var from = to;
                to += a;

                var itemData = new FortuneWheelItemData(item.Key, FortuneWheelRewards.GetRandom(item.Key), from, to);
                _itemsData.Add(itemData);

                var separator = Instantiate(_separatorPrefab, transform.GetChild(0));
                separator.SetPositionAndRotation(separator.position, Quaternion.Euler(0, 0, -from));
                separator.gameObject.SetActive(true);
                
                var widget = separator.GetComponent<UIWheelSeparator>();
                widget.Init(MainApp.GameResources.GetSprite(itemData.Item.Type, itemData.Item.Index), itemData.Item.Type.ToString());
                widget.Anchor.SetPositionAndRotation(widget.Anchor.position, Quaternion.Euler(0, 0, -Mathf.Lerp(itemData.FromAngle, itemData.ToAngle, .5f)));
            }
        }
        
        private FortuneWheelItemData GetRandomItem() => _itemsData[Random.Range(0, _itemsData.Count)];

        private static float FromItemToAngle(int index) => FULL_ANGLE * (index * .01f);
        
        public void _Rotate() => Rotate();

        private void Rotate()
        {
            if (_rotating) return;
            _rotating = true;

            if (_spinsCount <= 0)
            {
                Debug.LogError($"Spins is over!");
                return;
            }
            SpinCountChanged?.Invoke(--_spinsCount);
            
            _target = GetRandomItem();
            var angle = Random.Range(_target.FromAngle, _target.ToAngle);
            angle += _pointerOffset;
            if (angle >= FULL_ANGLE) angle -= FULL_ANGLE;
            var z = (FULL_ANGLE * MainApp.GameConfig.WheelTurnsCount) + (angle - _wheel.rotation.eulerAngles.z);
            var tween = _wheel.DORotate(new Vector3(0f, 0f, z), Config.WheelRotationTime, RotateMode.WorldAxisAdd);
            tween.SetEase(Config.WheelEase);
            tween.onComplete = OnRotationComplete;
        }

        private void OnRotationComplete()
        {
            _rotating = false;
            UIManager.Get<UIReward>().Show(_target.Item);
            WheelStop?.Invoke(_target.Item);
        }
    }
}