﻿namespace App.Scripts.FortuneWheel
{
    public class FortuneWheelItemData
    {
        public PlayerInventory.Item Item { get; }
        public float FromAngle  { get; }
        public float ToAngle  { get; }

        public FortuneWheelItemData(ItemType type, int index, float fromAngle, float toAngle)
        {
            Item = new PlayerInventory.Item(type, index);
            FromAngle = fromAngle;
            ToAngle = toAngle;
        }
    }
}