﻿using UnityEngine;

namespace App.Scripts
{
    public class BaseBehaviour: MonoBehaviour
    {
        private Transform _transform;
        // ReSharper disable once InconsistentNaming
        public new Transform transform => _transform == null ? _transform = base.transform : _transform;

        public Transform Transform => transform;
    }
}