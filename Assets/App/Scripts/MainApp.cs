using System;
using App.Scripts.ScriptableObjects;
using UnityEngine;

namespace App.Scripts
{
    public class MainApp : MonoBehaviour
    {
        public static GameConfig GameConfig { get; private set; }
        public static GameResources GameResources { get; private set; }
        public static PlayerInventory Inventory { get; private set; }
        
        private void Awake()
        {
            GameConfig = Resources.Load<GameConfig>("GameConfig");
            GameResources = Resources.Load<GameResources>("GameResources");

            Inventory = new PlayerInventory();
        }
    }
}