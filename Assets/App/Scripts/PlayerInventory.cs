﻿using System;
using System.Collections.Generic;
using App.Scripts.UI;
using UnityEngine;

namespace App.Scripts
{
    public class PlayerInventory
    {
        public struct Item
        {
            public ItemType Type { get; private set; }
            public int Index { get; private set; }

            public Item(ItemType type, int index)
            {
                Type = type;
                Index = index;
            }
        }
        
        public int Coins { get; private set; }
        private readonly List<Item> _items = new List<Item>();
        
        public static event Action<int> CoinsCountChanged;
        public static event Action<Item> ItemAdded;

        public PlayerInventory()
        {
            Coins = MainApp.GameConfig.StartupCoinsCount;
            UIReward.RewardAccepted += OnRewardAccepted;
        }
        
        public bool Contains(ItemType type, int index) => Contains(new Item(type, index));

        public bool Contains(Item item) => _items.Contains(item);

        private void OnRewardAccepted(Item item)
        {
            if (item.Type == ItemType.Coins)
            {
                AddCoins();
            }
            else
            {
                if (!_items.Contains(item))
                {
                    _items.Add(item);
                }
                else
                {
                    AddCoins();
                }
            }
            
            ItemAdded?.Invoke(item);
        }

        public bool TryBuyItem(ItemType type, int index)
        {
            if (Contains(type, index)) return false;
            var price = MainApp.GameConfig.ItemPrice;
            if (Coins < price) return false;

            Coins = Mathf.Clamp(Coins - price, 0, int.MaxValue);
            CoinsCountChanged?.Invoke(Coins);

            var item = new Item(type, index);
            _items.Add(item);
            ItemAdded?.Invoke(item);
            
            return true;
        }
        
        private void AddCoins()
        {
            Coins += MainApp.GameConfig.BonusCoinsCount;
            CoinsCountChanged?.Invoke(Coins);
        }
    }
}