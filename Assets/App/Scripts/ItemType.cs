﻿namespace App.Scripts
{
    public enum ItemType : byte
    {
        None,
        Coins,
        Hat,
        Robe,
        Skin
    }
}