using System.Collections.Generic;
using App.Scripts.UI;
using UnityEngine;

namespace App.Scripts
{
    public class LastRewards : MonoBehaviour
    {
        [SerializeField] private UIReward _uiRewardPrefab;
        private readonly Queue<UIReward> _queue = new Queue<UIReward>();

        private void Start()
        {
            PlayerInventory.ItemAdded += OnRewardReceived;        
        }

        private void OnDestroy()
        {
            PlayerInventory.ItemAdded -= OnRewardReceived;
        }

        private void OnRewardReceived(PlayerInventory.Item item)
        {
            if (_queue.Count >= MainApp.GameConfig.MaxLastRewardsCount) Destroy(_queue.Dequeue().gameObject);
            
            var reward = Instantiate(_uiRewardPrefab, transform);
            reward.transform.SetSiblingIndex(0);
            reward.Show(item);
            _queue.Enqueue(reward);
        }
    }
}
