﻿using System;
using App.Scripts.UI;
using UnityEngine;
using UnityEngine.UI;

namespace App.Scripts
{
    public class CharacterCustomizer: BaseBehaviour
    {
        [SerializeField] private Image _hat, _robe, _skin;

        private void Start()
        {
            UIGridItem.ItemSelected += ChangeItem;
        }

        private void OnDestroy()
        {
            UIGridItem.ItemSelected -= ChangeItem;
        }

        private void ChangeItem(ItemType type, int index)
        {
            switch (type)
            {
                case ItemType.Hat:
                    _hat.sprite = MainApp.GameResources.GetSprite(type, index);
                    break;
                case ItemType.Robe:
                    _robe.sprite = MainApp.GameResources.GetSprite(type, index);
                    break;
                case ItemType.Skin:
                    _skin.sprite = MainApp.GameResources.GetSprite(type, index);
                    break;
            }
        }
    }
}