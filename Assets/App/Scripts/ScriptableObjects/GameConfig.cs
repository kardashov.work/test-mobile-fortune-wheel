﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace App.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "ScriptableObjects/GameConfig", order = 1)]
    public class GameConfig : ScriptableObject
    {
        [Header("Wheel settings")] [SerializeField]
        private List<WheelItemKvp> _wheelItem;

        public List<WheelItemKvp> WheelItem => _wheelItem;


        [SerializeField] private float _wheelRotationTime;
        public float WheelRotationTime => _wheelRotationTime;


        [SerializeField] private Ease _wheelEase;
        public Ease WheelEase => _wheelEase;


        [SerializeField] private int _wheelTurnsCount = 5;
        public int WheelTurnsCount => _wheelTurnsCount;


        [SerializeField] private int _maxSpinCount = 99;
        public int MaxSpinCount => _maxSpinCount;
        
        
        [Header("Game settings")] [SerializeField]
        private int bonusCoinsCount = 100;
        public int BonusCoinsCount => bonusCoinsCount;
        

        [SerializeField] private int _itemPrice = 80;
        public int ItemPrice => _itemPrice;

        
        [SerializeField] private int _gridItemsCount = 30;
        public int GridItemsCount => _gridItemsCount;
        

        [SerializeField] private int _startupCoinsCount = 200;
        public int StartupCoinsCount => _startupCoinsCount;


        [SerializeField] private int _maxLastRewardsCount = 4;
        public int MaxLastRewardsCount => _maxLastRewardsCount;


        #region System

        [Serializable]
        public class WheelItemKvp : KeyValuePair<ItemType, int>
        {
            public WheelItemKvp(ItemType key, int value) : base(key, value)
            {
            }
        }

        [Serializable]
        public class KeyValuePair<TKey, TValue>
        {
            public TKey Key;
            public TValue Value;

            public KeyValuePair(TKey key, TValue value)
            {
                Key = key;
                Value = value;
            }
        }

        #endregion
    }
}