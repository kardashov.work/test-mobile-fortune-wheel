﻿using UnityEngine;

namespace App.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "GameResources", menuName = "ScriptableObjects/GameResources", order = 1)]
    public class GameResources: ScriptableObject
    {
        [SerializeField] private Sprite _coins;
        
        [SerializeField] private Sprite[] _hats;
        public Sprite[] Hats => _hats;
        
        [SerializeField] private Sprite[] _robes;
        public Sprite[] Robes => _robes;
        
        [SerializeField] private Sprite[] _skins;
        public Sprite[] Skins => _skins;

        public Sprite GetSprite(ItemType type, int index)
        {
            return type switch
            {
                ItemType.Coins => _coins,
                ItemType.Hat => Hats[index],
                ItemType.Robe => Robes[index],
                ItemType.Skin => Skins[index],
                _ => null
            };
        }
    }
}